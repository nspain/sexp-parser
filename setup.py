#!/usr/bin/env python3

from distutils.core import setup

setup(name="sexpparser",
      version="0.0",
      description="S-expression parser",
      author="Nick Spain",
      url="https://gitlab.com/nspain/sexpparser"
      )
