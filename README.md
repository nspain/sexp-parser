S-expression parser written in Python3.

To install:

    git clone https://gitlab.com/nspain/sexp-parser.git
    cd sexp-parser
    pip3 install -r requirements.txt

or

    pip3 install git+git://gitlab.com/nspain/sexp-parser.git

To use:

    import sexpparser

See docstrings for more detail info:

    python3 -c "import sexpparser; help(sexp)"

This library can be used as either as a CLI or imported as for your own scripts.

To use the CLI to get JSON:

    python3 sexp.py -json <sexp file>

This will give a text stream of valid JSON converted from sexps.
Do the same for XML except using the `xml` flag.

To use as a library just have a loook at the docstrings. They have info on how
the methods work and there are also some docstests that give you an idea of how
to use the functions.
