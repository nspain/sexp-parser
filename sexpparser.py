"""
S-expression (sexp) parser.
Features:
    Parse sexps into Python's internal representation
    Transcompile sexps into JSON
    Transcompile sexps into XML
With regards to transcompiling to JSON and XML, a string of the valid
serialisation is returned. This is an attempt to make the usage of this module
as broad but intuitive as possible because Python's JSON and XML libraries have
facilities to parse strings plus if you just want to convert to a JSON or XML
file you can just pipe it in there.
"""

# TODO:
#   [ ] Testing
#   [ ] Bug: Sexp nesting not working
#   [ ] Bug: First element is always an empty list

import sys


class SexpException(Exception):
    """ Define custom exception class, as is good Python style. """


class Sexp():
    """
    Python object representation of a sexp.

    When the object is initially created, a string is taken as an argument.
    This string is parsed using the private parse method. You can then use the
    to_json or to_xml methods to convert to JSON or XML respectively.
    Alternatively, just utilise the internal representation and work with sexps
    serialisation becasue you feel the pull of the parens...
    """
    represent = ""

    def parse(self, string):
        """
        Parse sexp string into Python's internal representation.
        """
        sexp = [[]]
        atom = ""
        for c in string:
            if c == "(":
                sexp.append([])
            elif c == ")":
                if atom:
                    # Try converting atom to integer or float
                    # This could probably do with a clean up, fastnumbers?
                    try:
                        atom = int(atom)
                    except ValueError:
                        try:
                            atom = float(atom)
                        except ValueError:
                            pass
                    sexp[-1].append(atom)
                    atom = ""
            elif c.isspace():
                if atom:
                    # Try converting atom to integer or float
                    # This could probably do with a clean up, fastnumbers?
                    try:
                        atom = int(atom)
                    except ValueError:
                        try:
                            atom = float(atom)
                        except ValueError:
                            pass
                    sexp[-1].append(atom)
                    atom = ""
            else:
                atom += c
        self.represent = sexp
        return sexp

    def to_json():
        """
        Return a string of valid JSON from a given STRING of valid sexp data.
        """
        pass

    def to_xml():
        """
        Return a string of valid XML from a given STRING of valid sexp data.
        """
        pass


def main(sexp_file):
    """ Main entry point for module. """
    sexp = Sexp()
    with open(sexp_file) as f:
        sexp.parse(f.read())
    print(sexp.represent)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        sys.exit(main(sys.argv[1]))
    else:
        import doctest
        doctest.testfile("test_sexpparser_doctest.txt")
